﻿using Microsoft.VisualBasic;

namespace VBTestApp.Domain
{
    public class DoThingClass
    {
        public DoThingClass()
        {
        }

        public void DoTheThing()
        {
            Interaction.MsgBox("Doing the Thing!", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly), "The thing");
        }
    }
}