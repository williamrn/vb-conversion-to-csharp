﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace VBTestApp.UI
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class UsrMainWindow : UserControl
    {

        // UserControl1 overrides dispose to clean up the component list.
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components is object)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _Button1 = new Button();
            _Button1.Click += new EventHandler(Button1_Click);
            SuspendLayout();
            // 
            // Button1
            // 
            _Button1.Location = new Point(282, 159);
            _Button1.Name = "_Button1";
            _Button1.Size = new Size(224, 98);
            _Button1.TabIndex = 0;
            _Button1.Text = "Do Thing!";
            _Button1.UseVisualStyleBackColor = true;
            // 
            // UsrMainWindow
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_Button1);
            Name = "UsrMainWindow";
            Size = new Size(800, 450);
            ResumeLayout(false);
        }

        private Button _Button1;

        internal Button Button1
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _Button1;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_Button1 != null)
                {
                    _Button1.Click -= Button1_Click;
                }

                _Button1 = value;
                if (_Button1 != null)
                {
                    _Button1.Click += Button1_Click;
                }
            }
        }
    }
}