﻿Imports VBTestApp.Domain

Public Class UsrMainWindow

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim rand As New Random()
        Dim randInt = rand.Next(1, 3)

        If (randInt = 1) Then
            DoThingOne()
        Else
            DoThingTwo()
        End If

    End Sub

    Private Sub DoThingOne()
        MsgBox("Hello world!", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Visual Basic Teste")
    End Sub

    Private Sub DoThingTwo()
        Dim thing As New DoThingClass()
        thing.DoTheThing()
    End Sub

End Class