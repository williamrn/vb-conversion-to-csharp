﻿
namespace VBTestApp.UI
{
    public partial class FrmMainForm
    {
        public FrmMainForm()
        {

            // This call is required by the designer.
            InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            Controls.Add(new UsrMainWindow());
        }
    }
}