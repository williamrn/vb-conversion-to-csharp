﻿using System;
using Microsoft.VisualBasic;
using VBTestApp.Domain;

namespace VBTestApp.UI
{
    public partial class UsrMainWindow
    {
        public UsrMainWindow()
        {
            InitializeComponent();
            _Button1.Name = "Button1";
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            var rand = new Random();
            int randInt = rand.Next(1, 3);
            if (randInt == 1)
            {
                DoThingOne();
            }
            else
            {
                DoThingTwo();
            }
        }

        private void DoThingOne()
        {
            Interaction.MsgBox("Hello world!", (MsgBoxStyle)((int)MsgBoxStyle.Critical + (int)MsgBoxStyle.OkOnly), "Visual Basic Teste");
        }

        private void DoThingTwo()
        {
            var thing = new DoThingClass();
            thing.DoTheThing();
        }
    }
}